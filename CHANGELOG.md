## 0.1.1 (July 25, 2017)

IMPROVEMENTS:
* Renamed bin script to be more descriptive

BUG FIXES:
* Fixed bug in suspended process


## 0.1.0 (July 17, 2017)

Initial version

