resource "null_resource" "trigger" {
  triggers = "${var.triggers}"

  provisioner "local-exec" {
    command = "python ${path.module}/bin/asg-update-notification ${var.asg_name} --region ${data.aws_region.current.name}"
  }
}
