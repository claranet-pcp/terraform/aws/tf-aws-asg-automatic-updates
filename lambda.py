import boto3
import os


ASG_NAME = os.environ['ASG_NAME']
MAX_SIZE = int(os.environ['MAX_SIZE'])
STRATEGY = os.environ['STRATEGY']

ROLLING_ADD_MODE = STRATEGY == 'rolling-add'

SCALING_PROCESSES_TO_SUSPEND = (
    'AlarmNotification',
    'AZRebalance',
    'ScheduledActions',
)


autoscaling = boto3.client('autoscaling')


class AutoScalingGroup(object):

    def __init__(self, data):
        self._data = data
        self.name = data['AutoScalingGroupName']

    def __getitem__(self, key):
        return self._data[key]

    def is_settled(self):
        """
        Checks if the desired number of instances are running and healthy.

        """

        instances = self['Instances']
        desired_capacity = self['DesiredCapacity']

        if len(instances) != desired_capacity:
            return False

        for instance in instances:
            if instance.get('LifecycleState') != 'InService':
                return False
            if instance.get('HealthStatus') != 'Healthy':
                return False

        return True

    def old_instances(self):
        """
        Returns all instances with a different Launch Configuration.

        Note: When an ASG has its Launch Configuration changed, even when
        temporarily changed and then changed back, all of the instances will
        be missing their value for Launch Configuration afterwards. Instances
        that are detached and reattached probably also lose their Launch
        Configuration value, but this has not been tested.

        """

        result = []
        for instance in self['Instances']:
            if instance.get('LaunchConfigurationName') != self['LaunchConfigurationName']:
                result.append(instance)
        return result

    def remove_instance(self, instance):
        """
        Marks an instance as unhealthy so the ASG will terminate it.

        """

        response = autoscaling.set_instance_health(
            InstanceId=instance['InstanceId'],
            HealthStatus='Unhealthy',
            ShouldRespectGracePeriod=False,
        )
        if response['ResponseMetadata']['HTTPStatusCode'] != 200:
            raise Exception('ERROR: {}'.format(response))

    def resume_processes(self):
        """
        Resumes all scaling processes.

        """

        response = autoscaling.resume_processes(
            AutoScalingGroupName=self.name,
        )
        if response['ResponseMetadata']['HTTPStatusCode'] != 200:
            raise Exception('ERROR: {}'.format(response))

    def suspend_processes(self, scaling_processes):
        """
        Suspends the supplied scaling processes.

        """

        response = autoscaling.suspend_processes(
            AutoScalingGroupName=self.name,
            ScalingProcesses=scaling_processes,
        )
        if response['ResponseMetadata']['HTTPStatusCode'] != 200:
            raise Exception('ERROR: {}'.format(response))

    def suspended_processes(self):
        """
        Returns the currently suspended processes.

        """

        result = set()
        for process in self['SuspendedProcesses']:
            result.add(process['ProcessName'])
        return result

    def update(self, **settings):
        """
        Updates the ASG with new settings.

        """

        response = autoscaling.update_auto_scaling_group(
            AutoScalingGroupName=self.name,
            **settings
        )
        if response['ResponseMetadata']['HTTPStatusCode'] != 200:
            raise Exception('ERROR: {}'.format(response))


def get_asg(asg_name):
    """
    Returns an Auto Scaling Group matching the name.

    """

    response = autoscaling.describe_auto_scaling_groups(
        AutoScalingGroupNames=[asg_name],
    )
    if response['ResponseMetadata']['HTTPStatusCode'] != 200:
        raise Exception('ERROR: {}'.format(response))

    asgs = response['AutoScalingGroups']
    if not asgs:
        raise Exception('Could not find ASG with name {}'.format(asg_name))

    return AutoScalingGroup(asgs[0])


def lambda_handler(event, context):
    """
    Lambda handler for autoscaling events, or direct invocations.

    """

    print(event)

    asg = get_asg(ASG_NAME)
    old_instances = asg.old_instances()

    if old_instances:
        suspended_processes = asg.suspended_processes()
        for process in SCALING_PROCESSES_TO_SUSPEND:
            if process not in suspended_processes:
                print('suspending scaling processes')
                asg.suspend_processes(SCALING_PROCESSES_TO_SUSPEND)
                break

    if not asg.is_settled():

        print('waiting for ASG to settle')
        for instance in asg['Instances']:
            instance_id = instance['InstanceId']
            if instance.get('LifecycleState') == 'InService' and instance.get('HealthStatus'):
                instance_state = instance['HealthStatus']
            else:
                instance_state = instance.get('LifecycleState') or 'unknown'
            print('{} state is {}'.format(instance_id, instance_state))

    elif ROLLING_ADD_MODE and old_instances and asg['MaxSize'] <= MAX_SIZE:

        print('increasing capacity to add new instance')
        asg.update(
            DesiredCapacity=asg['DesiredCapacity'] + 1,
            MaxSize=MAX_SIZE + 1,
        )

    elif len(old_instances) <= 1 and asg['MaxSize'] > MAX_SIZE:

        if old_instances:
            print('decreasing capacity to remove last old instance')
        else:
            print('decreasing capacity')
        asg.update(
            DesiredCapacity=asg['DesiredCapacity'] - 1,
            MaxSize=MAX_SIZE,
        )

    elif old_instances:

        print('removing old instance to have it replaced')
        asg.remove_instance(old_instances[0])

    elif asg['SuspendedProcesses']:

        print('resuming scaling processes')
        asg.resume_processes()
        print('update complete')

    else:

        print('nothing to do')
