variable "asg_name" {
  description = "The name of the Auto Scaling Group to target for automatic updates"
  type        = "string"
}

variable "asg_max_size" {
  description = "The max_size of the Auto Scaling Group"
  type        = "string"
}

variable "schedule" {
  description = "The CloudWatch Events scheduling expression"
  type        = "string"
  default     = ""
}

variable "strategy" {
  description = "Update strategy, one of: rolling-add, rolling-remove"
  type        = "string"
}

variable "triggers" {
  description = "Optionally trigger updates from Terraform"
  type        = "map"
  default     = {}
}

variable "suffix" {
  description = "The suffix to use for resource names"
  type        = "string"
  default     = "asg-automatic-updates"
}
