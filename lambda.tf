module "lambda" {
  source = "../tf-aws-lambda"

  function_name = "${var.asg_name}-${var.suffix}"
  description   = "Replaces instances within an ASG to use the current Launch Configuration."
  handler       = "lambda.lambda_handler"
  runtime       = "python3.6"
  timeout       = 10

  source_path = "${path.module}/lambda.py"

  attach_policy = true
  policy        = "${data.aws_iam_policy_document.lambda.json}"

  environment {
    variables = {
      ASG_NAME = "${var.asg_name}"
      MAX_SIZE = "${var.asg_max_size}"
      STRATEGY = "${var.strategy}"
    }
  }
}

data "aws_iam_policy_document" "lambda" {
  statement {
    effect = "Allow"

    actions = [
      "autoscaling:ResumeProcesses",
      "autoscaling:SetInstanceHealth",
      "autoscaling:SuspendProcesses",
      "autoscaling:UpdateAutoScalingGroup",
    ]

    resources = [
      "arn:aws:autoscaling:${data.aws_region.current.name}:${data.aws_caller_identity.current.account_id}:autoScalingGroup:*:autoScalingGroupName/${var.asg_name}",
    ]
  }

  statement {
    effect = "Allow"

    actions = [
      "autoscaling:DescribeAutoScalingGroups",
    ]

    resources = [
      "*",
    ]
  }
}
