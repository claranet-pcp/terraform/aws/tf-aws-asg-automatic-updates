# tf-aws-asg-automatic-updates

This module automatically replaces old instances when an Auto Scaling Group's Launch Configuration changes. In other words: rolling AMI updates.

The instance update process can be started with any/all of the following:

* regular checks according to a [CloudWatch Event Schedule Expression](http://docs.aws.amazon.com/AmazonCloudWatch/latest/events/ScheduledEvents.html)
* during `terraform apply`
* manually with `bin/asg-update`

## Use cases

>You're using Terraform and have found that updating an Auto Scaling Group's AMI does not affect the running instances. Other solutions don't perform *rolling* updates, but this module does.

>You're using another deployment method to change the ASG's AMI but don't want to deal with the rolling update process directly.

## Options

### Schedule

The `schedule` variable should be a [CloudWatch Event Schedule Expression](http://docs.aws.amazon.com/AmazonCloudWatch/latest/events/ScheduledEvents.html), or it can be blank to disable regular checks.

Using a schedule will increase the reliability of this module, as it effectively adds retry behaviour.

### Strategy

The `strategy` variable is used to determine the update behaviour.

#### Strategy: rolling-add

Temporarily increases the number of instances in the ASG during the update process. The `max_size` variable is used to restore the size afterwards.

This strategy is not suitable for "singleton" instances, as it will result in 2 instances running simultaneously. For other types of instances (regular auto scaling groups), choose this strategy.

Example for an ASG with 2 instances:

1. Lambda increases ASG size, ASG launches new instance
1. Lambda terminates old instance, ASG launches new instance
1. Lambda terminates old instance, ASG launches new instance
1. Lambda decreases ASG size, ASG terminates old instance

#### Strategy: rolling-remove

Simply terminates old instances and lets the ASG replace them.

This strategy will result in downtime if there is only 1 instance in the ASG.

Example for an ASG with 2 instances:

1. Lambda terminates old instance, ASG launches new instance
1. Lambda terminates old instance, ASG launches new instance

### Triggers

The `triggers` variable can be used to automatically trigger an update during `terraform apply`. The Launch Configuration ID should work in most cases.

If triggers are not defined then the update will be triggered during the first `terraform apply`, but not afterwards.

## Caution

__Use this module with caution; it terminates healthy instances.__

Care must be taken when using this module with certain types of instances, such as RabbitMQ and Elasticsearch cluster nodes. If these instances are using only ephemeral storage, then terminating them too quickly could result in data loss.

In this situation, use Auto Scaling Lifecycle Hooks on the instances to wait until everything is truly healthy (e.g. cluster status green) before putting the instance "in service".

## Usage

```
module "web_asg_auto_updates" {
  source = "tf-aws-asg-automatic-updates"

  asg_name     = "${aws_autoscaling_group.web.name}"
  asg_max_size = "${aws_autoscaling_group.web.max_size}"

  # Check for a new Launch Configuration every 5 minutes.
  schedule = "rate(5 minutes)"

  # Replace instances using this update strategy.
  strategy = "rolling-add"

  # Update instances immediately after Terraform changes the ASG's
  # Launch Configuation, rather than waiting for the schedule.
  triggers {
    launch_configuration_id = "${aws_launch_configuration.web.id}"
  }
}
```

## Dependencies

* [tf-aws-lambda](https://git.bashton.net/Bashton-Terraform-Modules/tf-aws-lambda)
  * tested against v0.4.1

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|:----:|:-----:|:-----:|
| asg_max_size | The max_size of the Auto Scaling Group | string | - | yes |
| asg_name | The name of the Auto Scaling Group to target for automatic updates | string | - | yes |
| schedule | The CloudWatch Events scheduling expression | string | `` | no |
| strategy | Update strategy, one of: rolling-add, rolling-remove | string | - | yes |
| suffix | The suffix to use for resource names | string | `asg-automatic-updates` | no |
| triggers | Optionally trigger updates from Terraform | map | `<map>` | no |
