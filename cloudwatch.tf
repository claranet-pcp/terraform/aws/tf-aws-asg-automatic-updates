# Trigger the Lambda function with these events.

resource "aws_cloudwatch_event_rule" "events" {
  name = "${var.asg_name}-${var.suffix}"

  event_pattern = <<PATTERN
{
  "source": [
    "asg.update",
    "aws.autoscaling"
  ],
  "detail-type": [
      "ASG Update Notification",
      "EC2 Instance Launch Successful",
      "EC2 Instance Terminate Successful"
  ]
}
PATTERN
}

resource "aws_cloudwatch_event_target" "events" {
  target_id = "${var.asg_name}-${var.suffix}-events"
  rule      = "${aws_cloudwatch_event_rule.events.name}"
  arn       = "${module.lambda.function_arn}"
}

resource "aws_lambda_permission" "events" {
  statement_id  = "${var.asg_name}-${var.suffix}-events"
  action        = "lambda:InvokeFunction"
  function_name = "${module.lambda.function_name}"
  principal     = "events.amazonaws.com"
  source_arn    = "${aws_cloudwatch_event_rule.events.arn}"
}

# Also check for changes regularly, if a schedule was provided.

resource "aws_cloudwatch_event_rule" "schedule" {
  count               = "${var.schedule == "" ? 0 : 1}"
  name                = "${var.asg_name}-${var.suffix}-schedule"
  schedule_expression = "${var.schedule}"
}

resource "aws_cloudwatch_event_target" "schedule" {
  count     = "${var.schedule == "" ? 0 : 1}"
  target_id = "${var.asg_name}-${var.suffix}-schedule"
  rule      = "${aws_cloudwatch_event_rule.schedule.name}"
  arn       = "${module.lambda.function_arn}"
}

resource "aws_lambda_permission" "schedule" {
  count         = "${var.schedule == "" ? 0 : 1}"
  statement_id  = "${var.asg_name}-${var.suffix}-schedule"
  action        = "lambda:InvokeFunction"
  function_name = "${module.lambda.function_name}"
  principal     = "events.amazonaws.com"
  source_arn    = "${aws_cloudwatch_event_rule.schedule.arn}"
}
